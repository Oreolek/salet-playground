$("document").ready(function() {
    // RUN Button
    $("#btnRun").click(function(event) {  
        event.preventDefault();
        
        var css    = ace.edit("css-editor").getSession().getValue();
        var script = ace.edit("js-editor").getSession().getValue();
        var html   = ace.edit("html-editor").getSession().getValue();
        var scss   = ""
        Sass.compile(css, function(result) {
          var previewDoc = window.frames[0].document;
          previewDoc.write("<!DOCTYPE html>");
          previewDoc.write("<html lang='en'>");
          previewDoc.write("<head>");
          previewDoc.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">');
          previewDoc.write('<link rel="stylesheet" href="css/salet.css">');
          previewDoc.write("<style type='text/css'>" + result.text + "</style>");
          previewDoc.write("</head><body>");
          previewDoc.write(html);
          previewDoc.write("<script src='https://code.jquery.com/jquery-1.12.4.min.js' type='text/javascript'></script>");
          previewDoc.write("<script src='https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.6/marked.min.js' type='text/javascript'></script>");
          previewDoc.write("<script src='https://cdn.jsdelivr.net/npm/salet@1.8.6/lib/index.min.js' type='text/javascript'></script>");
          previewDoc.write("<script type='text/javascript'>" + CoffeeScript.compile(script) + "</script>");
          previewDoc.write("</body>");
          previewDoc.write("</html>");
          previewDoc.close();
        });
    });
    
    // Preview code on page load
    $("#btnRun").click();
    
    // TIDYUP Button
    $("#btnTidyUp").click(function(event) {
        event.preventDefault();

        var beautify = ace.require("ace/ext/beautify"); 
        beautify.beautify(ace.edit("html-editor").getSession());
        beautify.beautify(ace.edit("css-editor").getSession());
        beautify.beautify(ace.edit("js-editor").getSession());
    });

    // Together Button
    $("#btnTogether").click(function(event) {
      event.preventDefault();

      TogetherJS(this);
      return false;
    });
});
